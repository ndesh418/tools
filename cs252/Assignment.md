# Assignments for CS252: July-Dec 2016.


## Assignment 1. (Aug 6, 2016)

All these assignments have to be demonstrated in front of the TA. You
will be asked questions on it.

1. Show the network parameters like IP address, netmask, default
   gateway etc of your cloud container. You need to show these
   parameters on the shell command line using appropriate shell
   commands. It is *not enough* to show it on you admin panel.

2. Find out all networking services are running on your system that
   uses some tcp or udp port.

3. Find the network route, i.e. the intermediate nodes in the network,
   that is taken by a packet sent from here to www.iitb.ac.in.

4. Block access to ssh on your machine form vyom.cc.iitk.ac.in


# Assignment 2. (Aug 16, 2016)

1. Create a internal lan inside the cloud with the following configuration

   * LAN A contains machine X and Y
   * LAN B contains machine Z
   * Select two distinct address space of the LANS.
   * Connect the two LANs with a router.

2. After logging in to X (via the web console), get the hardware address of Y and the router.

3. Find out what arping does. From the machine X, which of Y and Z
   will be accessible via `arping` and why ?

# Assignment 3. (Aug 23, 2016).

NOTE: Packets in the internal network gets dropped if its destination
does not fit into the internal lans subnet. So we are simplifying the
assignment.

Start by setting up a machine (let us call it `A`) which connects to our
cse network. You need to write iptable rules to do the following.

1. Ping from `A` to `vyom@cc` should not work where as ping from A to
   `turing@cse` should work.

2. Ping _from_ `vyom@cc` _to_ `A` should not work where as ping from
   `turing@cse` should work.

2. An ssh connection from vyom to A should be dropped where as ssh
   connection from `turing@cse` should function normally.

Note that all these should be achieved through IPtable rules and _not_
by other means.

# Assignment 4. (Sep 3, 2016)

In this assignment we will write scripts to build mail using the smtp protocol.

1. Checking mail from the IMAP server: use `openssl s_client
   <IMAP_HOST>` to connect to the CSE IMAP server. Use the IMAP SEARCH
   commands in [RFC 3501][RFC3501] to list all mails sent to you in
   August 2016 which have some attachment. (*Careful*: Try this in
   private. Your login credentials, even though encrypted over the
   channel, is visible as plaintext on the terminal!).

   Hint: Most shells allow reading passwords with out echoing (see the
   option `-s` for the read command). So if you want to prevent
   passwords displayed on terminal write a wrapper shell command that
   first reads your password using `read -s -p 'Password: '` and then
   passing it on to the openssl client.

2. Configure `msmtp`.


3. You are given a csv file with the fields: Name,email,Pan number, Donation. as given below.

        Foo Bar, foobar@example.com, ABCDE1234, 2000000
        Biz Bhur, biz.bhur@example.com, ABCDE4321, 200
        Baz Bis, bar.biz@hell.com, AXYZT1234, 20000000

      These are donations received by IIT Kanpur from various
      sources. Your job is to write a script to send a thank you message
      to them.  Write a shell script to send the mail to all the people
      on that list.

      * You can use `awk` to parse the csv file.

      * You can use the `mail` command to mails.

      * For quick templating, you can use the [`m4`][m4] macro language.

      A sample template is given in the repository at
      <https://bitbucket.org/ppk-teach/tools/src/master/cs252/smtp/>. Your
      program should replace `NAME_OF_USER`, `EMAIL_OF_USER`
      `PAN_OF_USER`, `DONTATION_OF_USER`, with the appropriate values
      from the CSV. Your program should be generic and should work
      with a different template with out any change to the script.

4. Write a replacement for the mail command that uses netcat and sends
   mail directly using the smtp server.

5. Generate a 80G letter (pdf) using latex template given in the
   source directory.  Send the generated pdf together with the text
   message as to the user. You will need to create mime-formated mails
   for this.

__Hints on m4:__ on using [`m4`][m4]. You can define macros on command
like using the `-D` option. If the template contains built in
[`m4`][m4] macros like `define` you can use the option `-P` which
ensures that all [`m4`][m4] builting macros acquire a prefix `m4_`,
i.e the macro `define` becomes `m4_define` which is less likely to
clash with the contents of your template.

You can get detailed information on [`m4`][m4] using the `info`
command. Also see the [`m4` tutorial][m4tutorial].

[m4tutorial]: <https://box.matto.nl/m4.html> "A quick intro to m4"
[m4]: <https://www.gnu.org/software/m4/m4.html>
[RFC3501]: <https://tools.ietf.org/html/rfc3501#section-6.4.4>

# Assignment 5. (September 19, 2016)

1. On an instance running on the cloud, configure a **null client** -
   a machine that can only send mail, not receive it. Once you have
   configured it, send an email to your cse account from the postfix
   instance. Basic postfix configuration is available at [the postfix
   website][postfixconfig].

     For this exercise, we recommend you start one of the ubuntu
   instances, install postfix, and run through the configuration
   options.

   	   * We recommend you to select "Internet Site",
	   * leave the domain name as such.
	   * In the "Other destinations" option, leave the box blank.
	   * For local networks, remove everything except 127.0.0.0/8
	   * Internet Protocols: "all"

      Tip: If you make mistakes, run `dpkg-reconfigure -plow postfix`
   to get the configuration menu again.
  
       Edit `/etc/postfix/main.cf` as necessary, figure out which
   relay host to use. If running postfix for the first time, run
   `/etc/init.d/postfix start`, afterwards for all further changes,
   run `/etc/init.d/postfix reload`.

2. The SSH transport layer takes care of establishing the secure
   connection.  Please refer to the RFC
   <https://www.ietf.org/rfc/rfc4253.txt>. Write a small program that
   connects to an ssh server and prints the following details about the server.

   	* Which version of ssh it runs
	* Which ciphers and hashes it supports
 	* What are the public key methods and its public keys
	* What Diffie-Hellman key exchange algorithms it supports.

	You should write your own program for this and not use a standard
    library.

	Hint: The client and server exchange packets and the initial
	packets are not encrypted. So a simple parser for packets should
	work fine. However, as a client, you might want to disable
	encryption to easy the initial chit-chat (you will otherwise have
	to use a library like zlib).

3. Write a version of `ssh-keyscan` which takes a list of ssh servers,
   connects to them, and generates a `known_hosts` file which contains
   the public keys of the server.  Make sure that you do not overwrite
   your own `known_hosts` by writing the output to the standard output.

[postfixconfig]:<http://www.postfix.org/BASIC_CONFIGURATION_README.html>


# Assignment 6 (September 26, 2015)

This assignment is about learning more about the HTTP protocol using
`curl`.


1. Find how to send `GET` requests using `curl`. 

2. Find how to send requests for the headers of websites alone. Find
   the top 10 websites using Alexa ranking lists. Write a shell-script
   using `curl` and output a list of servers used by the top
   website, and the caching policies they use.

3. Use `curl' to get iitk.ac.in, cse.iitk.ac.in and oars.cc.iitk.ac.in
   headers, listing the servers and caching policies.

4. Get the only first 50 KB of http://www.cse.iitk.ac.in. Can you do
   the same for http://www.iitk.ac.in?

5. Create a Google Form which is public. Read about how to do HTTP
   POST using `curl`. Post to the Google Form from the command line
   using `curl`.

6. Read about RESTful web services. Most large modern web applications
   provide REST APIs to interact with the application directly.

      Get a list of currently open issues at the course repository. In
   to do this, you need to do the following.
      * Find the URI to get the issues with the bitbucket REST API
      * Using HTTP GET, get the list of all the issues
      * Examine the data format to see how to filter the open issues.

7. One issue with Problem 4 is that we are not able to see the result
   of our POST request.

      Create an HTML form in your website. Write a
   simple PHP script which displays the data submitted in the
   form.

      Using the `requests` library in Python, POST to the HTML form,
   get the reponse and print it out.